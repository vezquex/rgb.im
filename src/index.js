import {createElement as h} from 'react'
import {render} from 'react-dom'
import App from './app'
import * as serviceWorker from './serviceWorker'

render(
	h(App),
	document.getElementById('root')
)

serviceWorker.unregister()
