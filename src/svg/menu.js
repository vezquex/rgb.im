import React from 'react'
export default
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 5 5">
<rect width="5" height="1" fill="#F88"></rect>
<rect y="2" width="5" height="1" fill="#8F8"></rect>
<rect y="4" width="5" height="1" fill="#88F"></rect>
</svg>