const db = require('nedb-promise')
const Log = db({filename: `data/log.db`, autoload: true })
const Room = db({filename: `data/room.db`, autoload: true })

const model = () => {
	const all = () => {
		return Room.cfind({on:true})
			.sort({name: 1})
			.exec()
	}
	const create = ({name}) => {
		const id = Math.random().toString(36).slice(2)
		const room = {
			id,
			k: name.toLowerCase(),
			name,
			on: true,
		}
		Room.insert(room)
		const r = {
			id,
		}
		return r
	}
	const del = ({id}) => {
		return Room.update({id},{$set:{on:false}})
	}
	const get = ({id}) => {
		return Log.cfind({room: id})
			.sort({published: -1})
			// .skip(Math.max(offset || OFFSET, 0))
			.limit(32)
			.exec()
			.then(log => ({id, log: log.reverse()}))
			.catch(console.log)
	}
	const getByName = async (name) => {
		const k = name.toLowerCase()
		return Room.findOne({k})
		.then(async r=>{
			const o = {}
			if(r && !r.on){
				o.create = true
				Room.update({k},{$set:{on:true}})
			}
			if(r){
				o.id = r.id
				o.log = (await get({id: r.id})).log
			}
			if(!r){
				const d = create({name})
				o.create = true
				o.id = d.id
			}
			return o
		})
		.catch(console.log)
	}
	const log = (m) => {
		m.published = Date.now()
		return Log.insert(m)
	}
	return {
		all,
		del,
		get,
		getByName,
		log,
	}
}

module.exports = model
