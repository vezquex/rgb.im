const {sortBy} = require('lodash')

const model = (state) => {
	if(!state.room) state.room = {
		get: {},
		byName: {},
	}
	const create = ({create, name}) => {
		const k = name.toLowerCase()
		if(state.room.byName[k]){
			throw 'Name taken'
		}
		const id = Math.random().toString(36).slice(2)
		const room = {
			id,
			log: [],
			name,
		}
		state.room.get[id] = state.room.byName[k] = room
		return {...room, create:true}
	}
	const get = ({id}) => {
		return state.room.get[id]
	}
	const getByName = (name) => {
		return state.room.byName[name.toLowerCase()] || create({name})
	}
	return {
		all(){
			return sortBy(state.room.get, o => o.name.toLowerCase())
		},
		create,
		get,
		getByName,
		log(m){
			const r = get(m.room)
			return r && r.log.push(m)
		},
	}
}

module.exports = model
