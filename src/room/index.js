import Linkify from 'react-linkify'
import {Buffer} from 'buffer'
import {Component, createElement as h} from 'react'
import {Consumer} from '../app/state';

const {name_length} = require('../config')

const name_width = (name_length+1)+'ch'
const name_width_style = {width:name_width}

export default class Room extends Component {
	state = {
		id: '',
		input: '',
		log: [],
	}
	componentDidMount(){
		const {props} = this
		const {room, socket} = props
		if(!socket) return
		socket.on('room/get/'+room, this.get)
		socket.emit('room/get', room)
	}
	componentWillUnmount(){
		const {room, socket} = this.props
		if(!socket) return
		socket.off('room/get/'+room)
		socket.off('room/log/'+this.state.id)
	}
	componentWillUpdate(nextProps){
		const {props, state} = this
		const {room, socket} = nextProps
		if(!socket) return
		if(room !== props.room){
			socket.off('room/get/'+props.room)
			state.id && socket.off('room/log/'+state.id)
			this.setState({log:[]})
			socket.on('room/get/'+room, this.get)
			socket.emit('room/get', room)
		}
		const el = this.chatEl
		if(el) this.shouldScrollBottom = el.scrollTop + el.offsetHeight + 1 >= el.scrollHeight
	}
	componentDidUpdate(){
		if(!this.shouldScrollBottom) return
		const el = this.chatEl
		if(el) el.scrollTop = el.scrollHeight
	}
	get = data => {
		if(!data) return
		const {id, log=[]} = data
		this.props.socket.off('room/log/'+id)
		this.props.socket.on('room/log/'+id, this.log)
		this.setState({id, log})
	}
	log = data => {
		this.setState(
			({log}) => ({log: log.concat(data)})
		)
	}
	handleInput = (event) => {
		this.setState({input: event.target.value})
	}
	handleName = (dispatch, event) => {
		dispatch({
			type: 'setState',
			state: {
				name: event.target.value,
			},
		})
	}
	chat = (name, event) => {
		event.preventDefault()
		const {socket} = this.props
		if(!socket){ return console.error('No room socket.') }
		socket.emit('room/log', {
			room: this.state.id,
			name,
			log: this.state.input,
		})
		this.setState({input:''})
	}
	color = (token) => {
		return Buffer(token, 'base64')[0] % 12
	}
	render(){
		const {state} = this
		const {input, log} = state
		return h(Consumer, null, ([context, dispatch])=>{
			const {name} = context
			return h('div', {className:'html column grow'},
				h('div',
					{
						className:'chat grow',
						ref: el => this.chatEl = el
					},
					log.map(({sid, name, log}, i) =>
						h('p', {key:i, className:['row', sid].join(' ')},
							h('span',
								{
									className:`name c${this.color(sid)}`,
									style: name_width_style,
									title:`${name} (${sid})`
								},
								name || '\u00a0'
							),
							h('span', {className:'divider'}, ' | '),
							h('span', {className:'log'},
								h(Linkify,
									{
										properties: {
											rel:'nofollow noreferrer',
											target:'_blank',
										},
									},
									log,
								),
							),
						)
					)
				),
				h('form',
					{
						className:'chat-form',
						onSubmit:ev=>this.chat(name,ev),
					},
					h('input', {
						accessKey:'n',
						className:'name-input',
						maxLength:name_length,
						onChange:(ev)=>this.handleName(dispatch, ev),
						placeholder:'Name',
						size:name_length,
						value:name,
					}),
					h('input', {placeholder:'Message', value:input, onChange:this.handleInput, required:true, noValidate:true, className:'submit grow valid', accessKey:'m'}),
					h('button', {className:'hide'}),
				),
			)
		})
	}
}
