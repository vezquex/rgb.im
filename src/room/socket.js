const {name_length} = require('../config')
const Model = require('./model-nedb')
const model = (state) => Model(state)
module.exports = {
	async all({action, socket, state}){
		const Room = model(state)
		socket.emit(action, await Room.all())
	},
	async get({action, data, io, socket, state}){
		const room = data
		const Room = model(state)
		const r = await Room.getByName(room)
		if(!r) return
		socket.emit(action+'/'+room, r)
		io.sockets.emit('room/log/'+room, {
			sid: 'Server',
			name: '[joined]',
			log: socket.id,
		})
		if(r.create){
			const all = await Room.all()
			io.sockets.emit('room/all', all)
		}
	},
	async del({action, data, io, socket, state}){
		const id = data
		const Room = model(state)
		const r = await Room.del({id})
		if(!r) return
		const {name} = r
		io.sockets.emit('room/log/'+name, {
			sid: 'Server',
			name: '[removed channel]',
			log: socket.id,
		})
		io.sockets.emit('room/all', await Room.all())
	},
	log({action, data, io, socket, state}){
		const {log, name='', room} = data
		const Room = model(state)
		const r = Room.get({id:room})
		if(!r) return
		const m = {
			sid: socket.id,
			name: name.slice(0, name_length),
			log,
			room,
		}
		Room.log(m)
		io.sockets.emit(action+'/'+room, m)
	},
}