import {Component, createElement as h} from 'react'

const {name_length} = require('../config')

export default class RoomBar extends Component {
	constructor(props){
		super(props)
		this.state = {
			name: this.props.name,
		}
	}
	componentWillUpdate(nextProps){
		const {name} = nextProps
		if(name !== this.props.name){
			this.setState({name})
		}
	}
	go = event => {
		event.preventDefault()
		this.props.clear && this.setState({name: ''})
		this.props.go(this.state.name)
	}
	handleName = event => {
		this.setState({name: event.target.value})
	}
	render(){
		const {props, state} = this
		const {name} = state
		const {className, link} = props
		return (
			h('form', {onSubmit:this.go, className},
				h('input', {
					accessKey:'r',
					className:'wide menu-item search valid',
					disabled:link,
					noValidate:true,
					onChange:this.handleName,
					placeholder:'Room',
					required:true,
					size:name_length,
					type:'search',
					value:name||'',
				}),
			)
		)
	}
}
