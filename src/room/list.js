import {Link} from 'react-router-dom'
import {Component, createElement as h} from 'react'

export default class RoomList extends Component {
	state = {
		all: 0,
		conf: '',
		data: [],
		disconnected: false,
	}
	componentDidMount(){
		const {socket} = this.props
		if(!socket) return
		socket.on('room/all', this.all)
		socket.emit('room/all')
		this.setState({
			dct: setTimeout(this.disconnected, 5e3),
		})
	}
	componentWillUnmount(){
		clearTimeout(this.state.dct)
		const {socket} = this.props
		socket && socket.off('room/all')
	}
	all = data => {
		this.setState({all:Date.now(), data, disconnected:false})
	}
	disconnected = () => {
		this.state.all || this.setState({disconnected:true})
	}
	del = (id) => {
		const {socket} = this.props
		socket && socket.emit('room/del', id)
	}
	render(){
		const {props, state} = this
		const {
			className = 'column RoomList',
			room,
		} = props
		const roomLower = room && room.toLowerCase()
		const {disconnected, data} = state
		return h('div', {className},
			disconnected && h('p', {className:'menu-item'}, '(Disconnected)'),
			Object.values(data).map(r => {
				return (
					h('div',
						{
							key:r.id,
							className:
								((roomLower === r.name.toLowerCase()) ? 'active' : '')
									+' row',
						},
						h(Link, {
								className:'menu-item grow',
								to:`/room/${r.name}`,
							},
							r.name,
						),
						h('button', {
								className:'a fade',
								onClick:()=>this.del(r.id),
								title:'Remove '+r.name,
							},
							'🗙',
						),
					)
				)
			}),
		)
	}
}
