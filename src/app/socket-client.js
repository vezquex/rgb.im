const socketClient = require('socket.io-client')
const {server} = require('../config')
const client = socketClient(server || '/', {path:'/socket'})
export default client
