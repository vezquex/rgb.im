import {createBrowserHistory} from 'history'

export const history = createBrowserHistory()

export const goToRoom = room => history.push('/room/'+room)
