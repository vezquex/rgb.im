import {createElement as h} from 'react'
import svgMenu from '../svg/menu'
import {title} from '../config'

export default h('div', {className:'logo'},
	h('span', {className:'icon-svg'}, svgMenu),
	' '+title,
)