import createContext from '../util/context'

const context = createContext({
	initialState: {
		name: '',
	},
	reducer(state, action){
		switch(action.type){
			case 'setState':
				return {
					...state,
					...action.state,
				}
			default:
				return state
		}
	},
})
export default context
export const {Consumer} = context
