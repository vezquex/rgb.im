import {createElement as h} from 'react'
import {history} from './history'
import {Route, Router} from 'react-router-dom'
import config from '../config'
import Home from '../page/home'
import Html from './html'
import Room from '../page/room'
import State from './state'

const {title} = config

const App = () => {
	return (
		h(State.Provider, null,
			h(Html, {title},
				h(Router, {history},
					h(Route, {exact:true, path:'/', component:Home}),
					h(Route, {exact:true, path:'/room/:room', component:Room}),
				)
			)
		)
	)
}

export default App
