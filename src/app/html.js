import Head from 'react-helmet'
import {createElement as h} from 'react'
import colorScheme from '../util/colorScheme'
import '../css/main.css'
import '../css/dark.css'
import '../css/light.css'
import '../css/red.css'
// import '../css/green.css'
// import '../css/blue.css'


export default (props) => {
	return (
		h(colorScheme.Provider, null,
			h(Main, props, props.children)
		)
	)
}

function Main(props){
	const {className, title} = props
	const [light] = colorScheme.use()
	return (
		h('div',
			{
				className:[
					'html',
					className,
					light ? 'light' : 'dark',
				].join(' '),
			},
			h(Head, null,
				title && h('title', null, title),
			),
			props.children,
		)
	)
}
