import {createElement as h} from 'react'
import {goToRoom as go} from '../app/history'
import RoomBar from '../room/bar'
import RoomList from '../room/list'
import config from '../config'
import logo from '../app/logotype'
import socket from '../app/socket-client'
import colorScheme from '../util/colorScheme'

const {title} = config

export default function(props){
	return h('div', {className:'home html column grow'},
		h('div', {className:'menu'},
			h('div', {className:'row'},
				h('span', {className:'menu-item home-logo grow'}, logo),
				h(colorScheme.Toggle, {
					className:'a fade2 colorSchemeToggle',
					style:{
						...colorScheme.Toggle.style,
						padding:'.25em .75em',
						width:'3em',
					},
				}),
			),
			h(RoomBar, {go}),
		),
		h(RoomList, {
			className:'menu scrollable',
			socket,
			title,
		}),
	)
}
