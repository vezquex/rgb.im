import {createElement as h} from 'react'
import {goToRoom as go} from '../app/history'
import {Link} from 'react-router-dom'
import colorScheme from '../util/colorScheme'
import config from '../config'
import Head from 'react-helmet'
import logo from '../app/logotype'
import Room from '../room/'
import RoomBar from '../room/bar'
import RoomList from '../room/list'
import socket from '../app/socket-client'
import svgMenu from '../svg/menu'

const {title} = config

export default (r) => {
	const {room} = r.match.params
	return h('div', {className:'room html row grow'},
		room && h(Head, null, h('title', null, room)),
		h('div', {className:'full-flex sidebar column border'},
			h('div', {className:'row'},
				h(Link, {to:'/', accessKey:'h', className:'menu-item colorless grow'},
					logo,
				),
				h(colorScheme.Toggle, {className:'a fade2 colorSchemeToggle'}),
			),
			h(RoomBar, {clear:true, go}),
			h(RoomList, {room, socket, title}),
		),
		h('div', {className:'column grow'},
			h('div', {className:'mobile room-bar menu row'},
				h(Link, {
					accessKey:'h',
					className:'colorless',
					title,
					to:'/',
					},
					h('div', {className:'icon-svg'}, svgMenu),
				),
				h(RoomBar, {go, name:room, className:'grow'}),
				h(colorScheme.Toggle, {
					className:'a fade2 colorSchemeToggle',
					style:{
						...colorScheme.Toggle.style,
						padding:'.75em',
						width:'3em',
					},
				}),
			),
			h(Room, {room, socket, title}),
		),
	)
}