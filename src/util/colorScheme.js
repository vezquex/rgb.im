import React, {createContext, createElement as h, useContext, useEffect, useState} from 'react'

const is_light = window.matchMedia('(prefers-color-scheme: light)').matches

const Context = createContext([
	[
		is_light,
		()=>console.error('colorScheme.Provider not ready.')
	],
	()=>{}
])

const Use = () => useContext(Context)
const use = Use

function Provider(props){
	const props_light = (props.value||{}).light
	const [light, set_light] = useState(
			(props_light !== undefined) ? props_light : is_light
	)
	useEffect(() => {
		const on_dark = ev => ev.matches && set_light(false)
		const on_light = ev => ev.matches && set_light(true)
		const media_dark = window.matchMedia('(prefers-color-scheme: dark)')
		const media_light = window.matchMedia('(prefers-color-scheme: light)')
		media_dark.addListener(on_dark)
		media_light.addListener(on_light)
		return ()=>{
			media_dark.removeListener(on_dark)
			media_light.removeListener(on_light)
		}
	}, [])
	useEffect(() => {
		window.document.body.classList.add(light ? 'light' : 'dark')
		window.document.body.classList.remove(light ? 'dark' : 'light')
	}, [light])
	return h(Context.Provider,
		{
			value: [light, set_light],
		}	,
		props.children
	)
}

function Toggle({
	className = 'colorSchemeToggle',
	darkLabel = <svg className="colorSchemeToggle-icon" viewBox="0 0 24 24"><path d="M20 8.69V4h-4.69L12 .69 8.69 4H4v4.69L.69 12 4 15.31V20h4.69L12 23.31 15.31 20H20v-4.69L23.31 12 20 8.69zM12 18c-.89 0-1.74-.2-2.5-.55C11.56 16.5 13 14.42 13 12s-1.44-4.5-3.5-5.45C10.26 6.2 11.11 6 12 6c3.31 0 6 2.69 6 6s-2.69 6-6 6z"></path><path d="M0 0h24v24H0V0z" fill="none"></path></svg>,
	//🔅🕯️🏴🌑☽🕶️
	lightLabel = <svg className="colorSchemeToggle-icon" viewBox="0 0 24 24"><path d="M20 8.69V4h-4.69L12 .69 8.69 4H4v4.69L.69 12 4 15.31V20h4.69L12 23.31 15.31 20H20v-4.69L23.31 12 20 8.69zm-2 5.79V18h-3.52L12 20.48 9.52 18H6v-3.52L3.52 12 6 9.52V6h3.52L12 3.52 14.48 6H18v3.52L20.48 12 18 14.48zM12 7c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5z"></path><path d="M0 0h24v24H0V0z" fill="none"></path></svg>,
	//🔆💡🏳️☀️☀🌣👓
	style = Toggle.style,
}){
	const [light, set_light] = use()
	const onClick = () => set_light(!light)
	return h('button',
		{
			className,
			onClick,
			style,
			title: light ? 'Dark' : 'Light',
		},
		light ? darkLabel : lightLabel,
	)
}
Toggle.style = {
	textAlign:'center',
	width: '2em',
}

export default {Toggle, Context, Provider, use}