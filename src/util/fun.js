const {omit} = require('lodash')

module.exports = {
	_id: (_, x) => x,
	_aid: ([_, x]) => x,
	assign: (a, b) => ({...a, ...b}),
	assignBy: k => (target, o) => ({...target, [o[k]]:o}),
	assignAllBy: k => (target, os) => os.reduce(
		(r, o) => {r[o[k]] = o; return r},
		{...target}),
	id: x => x,
	omit,// (o, paths)
	reject: (xs, v) => xs.filter(x => x !== v)
}