import {createContext, createElement as h, useContext, useReducer} from 'react'
export default function create({
	initialState,
	reducer,
}){
	const context = createContext()

	function Provider(props){
		return (
			h(context.Provider,
				{
					value: useReducer(reducer, initialState),
				},
				props.children
			)
		)
	}

	const Use = () => useContext(context)

	return {
		Consumer: context.Consumer,
		Provider,
		use: Use,
	}
}
