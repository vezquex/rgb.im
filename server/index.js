const config = require('../src/config')
const express = require('express')
const http = require('http')
const path = require('path')
const sockets = require('socket.io')
const {state} = require('./state')

const prod = !config.dev
const app = express()
const server = http.createServer(app)

if(prod){
	const build = path.join(__dirname, '../build')
	app.use(express.static(build))
	app.get('/*', (req, res)=>{
		res.sendFile(path.join(build, 'index.html'))
	})
}

const io = sockets(server, {
	path:'/socket',
	handlePreflightRequest: (req, res) => {
		res.writeHead(200, {
			'Access-Control-Allow-Headers': 'Content-Type, Authorization',
			'Access-Control-Allow-Origin': req.headers.origin,
			'Access-Control-Allow-Credentials': true,
		})
		res.end()
	},
})

const modules = ['room']
const controllers = modules.map(m => require(`../src/${m}/socket`))
io.on('connection', socket => {
	modules.forEach((module, i) => {
		Object.entries(controllers[i]).forEach(([m, method]) => {
			const action = module + '/' + m
			socket.on(action, data => method({action, data, io, socket, state}))
		})
	})
})

server.listen(config.port, (err) => {
	if(err){ throw err }
	console.log(`${config.server}`)
})
